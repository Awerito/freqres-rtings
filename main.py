import os
from pyfzf.pyfzf import FzfPrompt
from plumbum.commands.processes import ProcessExecutionError

from src.api import products_data
from src.fzf import find_headphone
from src.plt import parse_data_to_dataframe, term_graph


if __name__ == "__main__":

    print("Write exit to close the program")
    products = products_data()
    if not products:
        exit("Fail while retrieving products!")

    promp = FzfPrompt()
    hp = [p["fullname"] for p in products]
    hp.sort()
    hp.append("exit")

    try:
        while True:
            search = promp.prompt(hp)[0]
            if not search or search == "exit":
                break

            data = find_headphone(search, products)

            df = parse_data_to_dataframe(data)
            term_graph(df, data["fullname"])
            input("Press enter to close graph...")
    except ProcessExecutionError:
        pass
    except IndexError:
        pass

    os.system("cls" if os.name == "nt" else "clear")
    exit()
