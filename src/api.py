import httpx
from json import JSONDecodeError

HEADERS = {
    "Content-Type": "application/json",
    "Cookie": "pref-country=zz; headphones-store=auto; _rtings_session=Y1FWV0xSYlM3Y3pzS1cxVEZ6MVJRdUtKcWJoZHFxcFhzTzZnMjN3M0srTmEwMHd6M1BQR25oVGJBYzJTNHNIZTJXRGs1QzRpNTBvbmtkWVNYMUNxWTIzUWZJa1lqM2JRRUtOeFlzdVlQc0RFcUhkNjRDRUhkYlhFdVAyTWZmMGRIR0t4Y1ArWXFUQXVja1Q2MWZKSkRQakZacnRoQlRWS28xeE5iMEE4Z0xLb1UrRUF1eVAxZWpKbm42L2dvN1lyLS1lOElVNks0OHo3MjNrR1d6Q2hSTW1RPT0%3D--23ea0ccd2044a2c2bda5aeed56726f58e3c9a70d",
}


# TODO: Cookies get automatically
def products_data() -> list[dict]:
    """products_data returns all the products with his related info from the
    table_tool_products_list/ endpoint of the rtings api v2

    Returns
    -------
    list[dict]
        list of headphones with review on rtings.com

    """

    # That data just works, copy from curl command
    url = "https://www.rtings.com/api/v2/safe/table_tool__products_list"
    data = '{"variables":{"test_bench_ids":["90"],"named_version":"public","is_admin":false}}'
    response = httpx.post(url, data=data, headers=HEADERS)
    if response.status_code != 200:
        return None

    try:
        products = response.json()
    except JSONDecodeError:
        return {"error": "No json data found"}

    return products["data"]["products"]


def headphone_data(id: int) -> dict:
    """headphone_data fetch by id the Raw Frequency Response (RFR) and returns
    a dict object with the points of the graph

    Parameters
    ----------
    id: int
        ID of product from /table_tool_products_list api endpoint

    Returns
    -------
    dict
        Points for graph of RFR
    """

    # 7903 -> Raw Frequency Response endpoint
    url = "https://www.rtings.com/graph/data/{}/7903".format(id)

    response = httpx.get(url, headers=HEADERS)

    # TODO: use pandas for better formating
    if response.status_code != 200:
        return None

    data = response.json()
    del data["options"]

    return data


if __name__ == "__main__":

    # Products fetch
    products = products_data()
    print(len(products), "entries in https://rtings.com/")

    # # Product data fetch by id
    id = 1579
    data = headphone_data(id)
    print(f"Size of {id} data:", len(data["data"]), "points")
