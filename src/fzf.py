from src.api import products_data, headphone_data


def find_headphone(fullname, products):
    """find_headphone returns the graph data if is has an entry on rting, None otherwise.
    Also the products return is the products list for make use as cache

    Parameters
    ----------
    fullname: str
        query string of the headphone name
    products(optional): list[dict]
        cache for not repeat the product data requests

    Returns
    -------
    dict
        graph points data from the fullname query
    list[dict]
        product list for cache

    """

    product = next(p for p in products if p["fullname"] == fullname)
    data = headphone_data(product["id"])

    if not data:
        return None

    data["fullname"] = fullname
    data["id"] = product["id"]

    return data


if __name__ == "__main__":

    products = products_data()
    if not products:
        exit("Fail while retrieving products!")

    search = "Drop + THX Panda Wireless"
    data = find_headphone(search, products)

    print(data["id"], data["fullname"])
