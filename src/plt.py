import pandas as pd
import seaborn as sns
import plotext as plx
import matplotlib.pyplot as plt


def parse_data_to_dataframe(data: dict) -> pd.core.frame.DataFrame:
    """parse_data_to_dataframe returns a dataframe of the given data.

    Parameters
    ----------
    data: dict
        data fetched from https://rtings.com/

    Returns
    -------
    DataFrame
        Pandas dataframe of the given data with columns; "Frequency",
        "Target Response", "Left hear" and "Right hear".

    """

    df = pd.DataFrame(data["data"], columns=data["header"], dtype="float")

    if type(df.Left) == pd.core.frame.DataFrame:
        lefts = list()
        for _, vals in df.Left.iterrows():
            sum = vals.sum()
            if not vals.isna().values.any():
                sum /= 2
            lefts.append(sum)
        df["Left hear"] = pd.Series(lefts)
    elif type(df.Left) == pd.core.series.Series:
        df["Left hear"] = df.Left

    if type(df.Right) == pd.core.frame.DataFrame:
        rights = list()
        for _, vals in df.Right.iterrows():
            sum = vals.sum()
            if not vals.isna().values.any():
                sum /= 2
            rights.append(sum)
        df["Right hear"] = pd.Series(rights)
    elif type(df.Right) == pd.core.series.Series:
        df["Right hear"] = df.Right

    df.drop(["Left", "Right"], axis=1, inplace=True)

    return df


def show_graph(df: pd.core.frame.DataFrame, title="Frequency Response"):
    """[Deprecated] show_graph generate the curve of raw frequency response of
    the given dataframe and plots it on a separated windows.

    Parameters
    ----------
    df: DataFrame
        Data of the product to graph
    title (optiona): str
        Title of the graph

    """

    xticks_vals = [20, 60, 125, 250, 500, 1000, 2000, 5000, 10000, 20000]
    xticks_names = [20, 60, 125, 250, 500, "1k", "2k", "5k", "10k", "20k"]
    _, axis = plt.subplots(figsize=(11, 5))

    sns.lineplot(x="Frequency", y="Target Response", label="Target", data=df)
    sns.lineplot(x="Frequency", y="Left hear", label="Left", data=df)
    sns.lineplot(x="Frequency", y="Right hear", label="Right", data=df)

    axis.set(xscale="log")
    axis.legend()
    axis.grid()

    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Volume (dB)")
    plt.title(title)
    plt.xticks(ticks=xticks_vals, labels=xticks_names)
    plt.xlim((20, 20000))

    plt.show()


def term_graph(df: pd.core.frame.DataFrame, title="Frequency Response"):
    """term_graph generate the curve of raw frequency response of
    the given dataframe and plots it on the terminal windows

    Parameters
    ----------
    df: DataFrame
        Data of the product to graph
    title (optiona): str
        Title of the graph

    """

    xticks_vals = [20, 60, 125, 250, 500, 1000, 2000, 5000, 10000, 20000]
    xticks_names = [20, 60, 125, 250, 500, "1k", "2k", "5k", "10k", "20k"]

    plx.cld()  # Clear previews plots

    plx.plot(df["Frequency"], df["Right hear"], color="red", label="Right")
    plx.plot(df["Frequency"], df["Left hear"], color="blue", label="Left")
    plx.plot(df["Frequency"], df["Target Response"], color="green", label="Target")

    plx.xscale("log")
    plx.grid(horizontal=True, vertical=True)

    plx.xlabel("Frequency (Hz)")
    plx.ylabel("Volume (dB)")
    plx.title(title)
    plx.xticks(ticks=xticks_vals, labels=xticks_vals)
    # plx.xlim(left=20, right=20000)

    size = plx.terminal_size()
    plx.clear_terminal()  # Clean screen
    plx.plot_size(*size)  # User terminal windows size
    plx.show()


if __name__ == "__main__":

    from api import products_data
    from fzf import find_headphone

    print("Fetching products...")
    products = products_data()
    if not products:
        exit("Fail while retrieving products!")

    search = "Drop + THX Panda Wireless"
    data = find_headphone(search, products)

    df = parse_data_to_dataframe(data)
    # show_graph(df, data["fullname"])
    term_graph(df, data["fullname"])
